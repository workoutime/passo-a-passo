# passo-a-passo

## Acesso à aplicação
- Disponibilizei um ambiente na nuvem, caso não queiram baixar o projeto e executar localmente.
- Link de acesso: https://workoutime-frontend.herokuapp.com/

## Executando o projeto local

### Baixar o projeto do banco de dados postgres
- Acessar a pasta e executar: 
```
docker-compose up -d
```
- Dessa forma o banco de dados já estará executando.

#### Criar um banco de dados 
- Criar um banco de dados com o nome workoutime

#### Rodar as migrations para criação das tabelas
- yarn typeorm migration:run

- O arquivo ormconfig.json já está configurado com as credenciais de acesso para esse banco de dados.

### Utilizando o banco de dados da cloud
- Caso não queira executar o passo anterior e não ter de configurar um banco local,
enviei um email com o ormconfig.json com os dados de acesso do banco de dados da cloud.

### Instalar o yarn
https://yarnpkg.com/

### Baixar o projeto de backend
- Executar o yarn para instalação das dependências
```
yarn
```
- Executar yarn start para executar o frontend.
```
yarn start
```

### Baixar o projeto de frontend
- Executar o yarn para instalação das dependências
```
yarn
```
- Executar yarn start para executar o frontend.
```
yarn start
```

Acredito que é isso. Qualquer coisa entrem em contato.

# Observações
- Não deixei o design da tela responsivo. Então funcionará melhor na tela do computador.
- A regex de validação de email, não está 100%. Mas para a maioria dos emails que testei funcionou. Ex: teste@gmail.com
- Ao cadastrar um novo usuário o toast com a mensagem de sucesso não está aparecendo. Somente redireciona o usuaŕio para a página de login novamente.
- Há um bug no login do usuário.
- Esses foram pequenos bugs não resolvidos. Mas acredito que a funcionalidade principal do desafio foi atendida.

# Cadastro de um usuário
- Utilizar um email para cadastro tipo: nome@email.com
- Senha: 123456

# Usuário já cadastrado no sistema disponibilizado na nuvem
- login: atleta@gmail.com
- senha: 123456

# Fluxo de usuário
- Acessar a aplicação.
- Cadastrar-se.
- Fazer login.
- Incluir atividades físicas na sua lista de atividades.
- No header há a possibilidade de o usuário acessar o componente de gráfico.
- Esse componente apresentará o desempenho do atleta por dia em quantidade total de minutos.
- Fazer logout no header.


